package pl.gawronlucas.clockee

import android.app.Application
import net.danlew.android.joda.JodaTimeAndroid

/**
 * Created by Lucas on 2017-03-21.
 */
class ClockeeApp : Application() {
    override fun onCreate() {
        super.onCreate()
        JodaTimeAndroid.init(this)
    }
}