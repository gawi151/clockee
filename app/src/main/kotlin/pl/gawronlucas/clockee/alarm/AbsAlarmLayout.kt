package pl.gawronlucas.clockee.alarm

import android.support.v7.widget.RecyclerView
import android.view.View
import pl.gawronlucas.clockee.view.OnBindViewHolder
import pl.gawronlucas.clockee.view.OnRecycleViewHolder
import pl.gawronlucas.clockee.view.LayoutResourceProvider

/**
 * Created by Lucas on 2017-03-12.
 */
abstract class AbsAlarmLayout(itemView: View) : RecyclerView.ViewHolder(itemView), AlarmDetailView, OnBindViewHolder<AlarmViewModel>, OnRecycleViewHolder