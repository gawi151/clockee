package pl.gawronlucas.clockee.alarm

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

/**
 * Created by Lucas on 2017-03-12.
 */
class AlarmsAdapter : RecyclerView.Adapter<AbsAlarmLayout>() {

    private var alarmsList: MutableList<AlarmViewModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbsAlarmLayout {
        val view = LayoutInflater.from(parent.context).inflate(AlarmLayout.layoutId(), parent, false)
        return AlarmLayout(view)
    }

    override fun onBindViewHolder(holder: AbsAlarmLayout, position: Int) {
        holder.onBind(alarmsList[position])
    }

    override fun getItemCount(): Int = alarmsList.count()

    fun setAlarms(alarmsList: List<AlarmViewModel>, notify: Boolean = true) {
        this.alarmsList.clear()
        this.alarmsList.addAll(alarmsList)
        if (notify) notifyDataSetChanged()
    }

    fun getAllAlarms(): List<AlarmViewModel> = alarmsList

    fun getAlarm(position: Int): AlarmViewModel {
        return alarmsList[position]
    }

    fun addAlarm(alarmViewModel: AlarmViewModel, notify: Boolean = true) {
        val position = alarmsList.count()
        alarmsList.add(position, alarmViewModel)
        if (notify) notifyItemInserted(position)
    }

    fun removeAlarm(alarmViewModel: AlarmViewModel, notify: Boolean = true) {
        val position = alarmsList.indexOf(alarmViewModel)
        alarmsList.removeAt(position)
        if (notify) notifyItemRemoved(position)
    }
}