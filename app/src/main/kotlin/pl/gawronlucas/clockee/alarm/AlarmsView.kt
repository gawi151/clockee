package pl.gawronlucas.clockee.alarm

import pl.gawronlucas.clockee.view.View

/**
 * Created by Lucas on 2017-03-12.
 */
interface AlarmsView : View {
    fun addAlarm()
    fun removeAlarm()
    fun editAlarm()
    fun showTimePicker()
}