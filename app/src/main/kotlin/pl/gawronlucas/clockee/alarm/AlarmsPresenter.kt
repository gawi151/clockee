package pl.gawronlucas.clockee.alarm

import pl.gawronlucas.clockee.domain.Alarm
import pl.gawronlucas.clockee.view.Presenter

/**
 * Created by lucas on 09.05.17.
 */
interface AlarmsPresenter : Presenter<AlarmsView> {
    fun addAlarm(alarm: Alarm)
    fun createAlarm()
    fun removeAlarm(alarm: Alarm)
    fun updateAlarm(alarm: Alarm)
}