package pl.gawronlucas.clockee.alarm

import android.app.Activity
import android.media.RingtoneManager
import android.net.Uri
import android.view.View
import android.widget.*
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import pl.gawronlucas.clockee.R
import android.content.Intent
import org.jetbrains.anko.onCheckedChange
import pl.gawronlucas.clockee.domain.DayOfWeek
import java.util.*


/**
 * Created by Lucas on 2017-03-12.
 */
class AlarmLayout(itemView: View) : AbsAlarmLayout(itemView) {

    var timeView: TextView = itemView.find(R.id.alarmTime)
    var alarmSwitch: Switch = itemView.find(R.id.alarmSwitch)
    var alarmSummary: TextView = itemView.find(R.id.alarmSummary)
    var repeatAlarmCheck: CheckBox = itemView.find(R.id.repeatAlarmCheck)
    var daysBtns: LinearLayout = itemView.find(R.id.repeatAlarmCheck)
    var alarmSoundBtn: TextView = itemView.find(R.id.alarmSoundBtn)
    var vibrationCheck: CheckBox = itemView.find(R.id.vibrationCheck)
    var alarmLabel: TextView = itemView.find(R.id.labelBtn)
    var deleteBtn: TextView = itemView.find(R.id.deleteBtn)
    var detailsArrow: ToggleButton = itemView.find(R.id.detailsArrow)

    companion object {
        fun layoutId(): Int = R.layout.item_alarm
    }

    override fun onBind(item: AlarmViewModel) {
        val alarm = item.alarm

        timeView.text = item.getFormattedTime()
        timeView.onClick {
            /* show alarm time picker */
        }
        alarmSwitch.isChecked = alarm.enabled
        alarmSwitch.onCheckedChange { compoundButton, checked ->
            if (checked) enableAlarm() else disableAlarm()
        }
        alarmSummary.text = item.getAlarmSummary()
        val alarmPeriod = alarm.period
        val isRepeatable = alarmPeriod.isRepeatable()
        repeatAlarmCheck.isChecked = isRepeatable
        repeatAlarmCheck.onCheckedChange { compoundButton, checked ->
            if (checked) alarm.period.days = EnumSet.allOf(DayOfWeek::class.java)
            else alarm.period.days.clear()
        }
        if (isRepeatable) {
            alarmPeriod.days.forEach { day ->
                (daysBtns.getChildAt(day.num) as CompoundButton).isChecked = true
            }
        }

        val ringtoneUri = if (alarm.ringtoneUriString.isNotBlank()) {
            Uri.parse(alarm.ringtoneUriString)
        } else {
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        }

        val ctx = itemView.context

        val ringtone = RingtoneManager.getRingtone(ctx, ringtoneUri)
        ringtone?.let { alarmSoundBtn.text = it.getTitle(ctx) }
        alarmSoundBtn.onClick {
            val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM)
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone")
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, ringtoneUri)
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false)
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
            (ctx as Activity).startActivityForResult(intent, 909)
        }

        vibrationCheck.isChecked = alarm.vibration
        vibrationCheck.onCheckedChange { compoundButton, checked ->
            alarm.vibration = checked
        }
        alarmLabel.text = alarm.name
        alarmLabel.onClick { /* todo show dialog with label edit text */ }
        deleteBtn.onClick { /* todo add delete action */ }
        detailsArrow.onCheckedChange { compoundButton, checked ->
            if (checked) {
                showMore()
                hideAlarmSummary()
            } else {
                hideMore()
                showAlarmSummary()
            }
        }
        itemView.onClick { detailsArrow.isChecked = !detailsArrow.isChecked }
    }

    override fun onRecycle() {
    }

    override fun showAlarmSummary() {
        alarmSummary.visibility = View.VISIBLE
    }

    override fun hideAlarmSummary() {
        alarmSummary.visibility = View.GONE
    }

    override fun deleteAlarm() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showMore() {

    }

    override fun hideMore() {

    }

    override fun showDaysOfWeek() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideDaysOfWeek() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun enableAlarm() {

    }

    override fun disableAlarm() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun changeSound() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun changeLabel() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun changeTime() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun turnOnVibration() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun turnOffVibration() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun selectDay(day: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deselectDay(day: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}