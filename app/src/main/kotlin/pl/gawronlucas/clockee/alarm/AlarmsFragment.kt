package pl.gawronlucas.clockee.alarm

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.gawronlucas.clockee.MainScreenFragment
import pl.gawronlucas.clockee.R

/**
 * Created by Lucas on 2017-03-12.
 */
class AlarmsFragment : MainScreenFragment(), AlarmsView {

    lateinit var alarmsList: RecyclerView
    lateinit var alarmsAdapter: AlarmsAdapter
    lateinit var alarmsPresenter: AlarmsPresenter

    companion object {
        fun newInstance(): AlarmsFragment {
            val frag = AlarmsFragment()
            return frag
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_alarms, container, false)
        alarmsAdapter = AlarmsAdapter()
        alarmsList = view.findViewById(R.id.alarmsList) as RecyclerView
        alarmsList.adapter = alarmsAdapter
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun getTabTextId(): Int = R.string.tab_alarm

    override fun getTabIconId(): Int = R.drawable.ic_alarm_white_24dp

    override fun addAlarm() {
        alarmsPresenter.createAlarm()
    }

    override fun showTimePicker() {

    }

    override fun removeAlarm() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun editAlarm() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFloatingAction() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getFloatingActionIconId(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showFloatingAction() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideFloatingAction() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}