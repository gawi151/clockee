package pl.gawronlucas.clockee.alarm

/**
 * Created by Lucas on 2017-03-20.
 */
interface AlarmDetailView {
    fun deleteAlarm()
    fun showMore()
    fun hideMore()
    fun showDaysOfWeek()
    fun hideDaysOfWeek()
    fun enableAlarm()
    fun disableAlarm()
    fun changeSound()
    fun changeLabel()
    fun changeTime()
    fun turnOnVibration()
    fun turnOffVibration()
    fun selectDay(day: Int) // from 0 to 6
    fun deselectDay(day: Int) // from 0 to 6
    fun showAlarmSummary()
    fun hideAlarmSummary()
}