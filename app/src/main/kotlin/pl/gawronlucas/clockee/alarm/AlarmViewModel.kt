package pl.gawronlucas.clockee.alarm

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import org.jetbrains.anko.append
import org.joda.time.DateTime
import pl.gawronlucas.clockee.domain.Alarm

/**
 * Created by Lucas on 2017-03-13.
 */
data class AlarmViewModel(val alarm: Alarm, var showDetails: Boolean = false) {
    fun getFormattedTime(): String = "${alarm.time}"

    fun getAlarmSummary(): Spannable {
        val spannableAlarmSummary = SpannableStringBuilder()
        if (alarm.name.isNotBlank()) {
            spannableAlarmSummary.append(alarm.name, ForegroundColorSpan(Color.DKGRAY))
            spannableAlarmSummary.append(" ")
        }
        val timeColorSpan = ForegroundColorSpan(Color.BLACK)
        if (alarm.period.isRepeatable()) {
            spannableAlarmSummary.append(alarm.period.toString(), timeColorSpan)
        } else {
            val alarmDate = DateTime()
                    .withHourOfDay(alarm.time.hour)
                    .withMinuteOfHour(alarm.time.minutes)
            val timeString = if (alarmDate.isAfterNow || alarmDate.isEqualNow)
                "Tomorrow"
            else "Today"
            spannableAlarmSummary.append(timeString, timeColorSpan)
        }

        return spannableAlarmSummary
    }
}
