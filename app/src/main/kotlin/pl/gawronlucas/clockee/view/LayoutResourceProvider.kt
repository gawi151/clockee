package pl.gawronlucas.clockee.view

import android.support.annotation.LayoutRes

/**
 * Created by Lucas on 2017-03-13.
 */
interface LayoutResourceProvider {
    @LayoutRes fun layoutId(): Int
}