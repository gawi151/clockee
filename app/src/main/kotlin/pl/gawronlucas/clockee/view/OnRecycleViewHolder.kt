package pl.gawronlucas.clockee.view

/**
 * Created by Lucas on 2017-03-13.
 */
interface OnRecycleViewHolder {
    fun onRecycle()
}