package pl.gawronlucas.clockee.view

/**
 * Created by Lucas on 2017-01-02.
 */
interface Presenter<in T : View> {
    fun bindView(view: T)
    fun unbindView()
}