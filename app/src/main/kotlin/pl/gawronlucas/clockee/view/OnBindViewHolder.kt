package pl.gawronlucas.clockee.view

/**
 * Created by Lucas on 2017-03-13.
 */
interface OnBindViewHolder<in T> {
    fun onBind(item: T)
}