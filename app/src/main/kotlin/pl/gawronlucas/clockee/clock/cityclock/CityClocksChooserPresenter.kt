package pl.gawronlucas.clockee.clock.cityclock

import pl.gawronlucas.clockee.view.Presenter

/**
 * Created by lucas on 09.05.17.
 */
interface CityClocksChooserPresenter : Presenter<CityClocksChooserView>