package pl.gawronlucas.clockee.clock

import pl.gawronlucas.clockee.view.Presenter

/**
 * Created by lucas on 09.05.17.
 */
interface ClocksPresenter : Presenter<ClocksView>