package pl.gawronlucas.clockee.clock

import pl.gawronlucas.clockee.view.View

/**
 * Created by lucas on 09.05.17.
 */
interface ClocksView : View {
    fun showClocks(clocks: List<String>)
    fun openCityClockChooser()
}