package pl.gawronlucas.clockee.clock

import pl.gawronlucas.clockee.MainScreenFragment
import pl.gawronlucas.clockee.R

/**
 * Created by lucas on 09.05.17.
 */
class ClocksFragment : MainScreenFragment(), ClocksView {

    companion object {
        fun newInstance(): ClocksFragment {
            val frag = ClocksFragment()
            return frag
        }
    }

    override fun getTabTextId(): Int = R.string.tab_clock

    override fun getTabIconId(): Int = R.drawable.ic_access_time_white_24dp

    override fun showClocks(clocks: List<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openCityClockChooser() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFloatingAction() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getFloatingActionIconId(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showFloatingAction() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideFloatingAction() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}