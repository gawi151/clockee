package pl.gawronlucas.clockee

import android.content.res.Resources
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

/**
 * Created by lucas on 04.05.17.
 */
class MainTabsAdapter<out T : MainScreenFragment>(val screenList: List<T> = listOf(), var resources: Resources, fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return screenList[position]
    }

    @Suppress("UNCHECKED_CAST")
    fun getFragment(position: Int): T {
        return getItem(position) as T
    }

    override fun getCount(): Int = screenList.count()

    override fun getPageTitle(position: Int): CharSequence {
        val pageTitleId = getFragment(position).getTabTextId()
        return resources.getString(pageTitleId)
    }
}