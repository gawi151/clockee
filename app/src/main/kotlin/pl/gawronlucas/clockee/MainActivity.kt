package pl.gawronlucas.clockee

import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import pl.gawronlucas.clockee.alarm.AlarmsFragment
import pl.gawronlucas.clockee.clock.ClocksFragment

class MainActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    lateinit var toolbar: Toolbar
    lateinit var tabs: TabLayout
    lateinit var tabsPager: ViewPager
    lateinit var tabsAdapter: MainTabsAdapter<MainScreenFragment>
    lateinit var fab: FloatingActionButton
    lateinit var screens: List<MainScreenFragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViews()
        prepareScreens()
        prepareTabs()
        prepareToolbar()
        prepareFab()
    }

    private fun findViews() {
        toolbar = find(R.id.toolbar)
        tabs = find(R.id.tabs)
        tabsPager = find(R.id.tabsPager)
    }

    fun prepareScreens() {
        screens = listOf<MainScreenFragment>(AlarmsFragment.newInstance(),
                ClocksFragment.newInstance(),
                AlarmsFragment.newInstance(),
                ClocksFragment.newInstance())
    }

    private fun prepareTabs() {
        tabsAdapter = MainTabsAdapter(screens, resources, supportFragmentManager)
        tabsPager.adapter = tabsAdapter
        tabsPager.addOnPageChangeListener(this)
        tabs.setupWithViewPager(tabsPager, true)
        for (i in 0 until tabs.tabCount) {
            tabs.getTabAt(i)?.setIcon(screens[i].getTabIconId())
        }
    }

    private fun prepareToolbar() {
        setSupportActionBar(toolbar)
    }

    private fun prepareFab() {
        fab = find(R.id.floatingActionBtn)
        fab.onClick {
            screens[tabsPager.currentItem].onFloatingAction()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onDestroy() {
        tabsPager.removeOnPageChangeListener(this)
        super.onDestroy()
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
    }

    interface OnScreenInteractionListener {
        fun onFloatingAction()
        @DrawableRes fun getFloatingActionIconId(): Int
        fun showFloatingAction()
        fun hideFloatingAction()
    }
}