package pl.gawronlucas.clockee

import android.support.annotation.DrawableRes
import android.support.v4.app.Fragment

/**
 * Created by lucas on 05.05.17.
 */
abstract class MainScreenFragment : Fragment(), MainActivity.OnScreenInteractionListener {
    abstract fun getTabTextId(): Int
    @DrawableRes abstract fun getTabIconId(): Int
}