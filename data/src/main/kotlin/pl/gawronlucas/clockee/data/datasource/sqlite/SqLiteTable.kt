package pl.gawronlucas.clockee.data.datasource.sqlite

/**
 * Created by lucas on 02.05.17.
 */
interface SqLiteTable {
    fun createStatement(): String
    fun updateStatement(): String
    fun columns(): Array<String>
    fun name(): String

    companion object {
        const val PRIMARY_KEY = "PRIMARY KEY"
        const val INTEGER = "INTEGER"
        const val TEXT = "TEXT"
        const val CREATE_TABLE = "CREATE TABLE"
        const val ALTER_TABLE = "ALTER TABLE"
        const val DROP_TABLE = " DROP TABLE"
        const val IF = "IF"
        const val IN = "IN"
        const val EXISTS = "EXISTS"
    }
}