package pl.gawronlucas.clockee.data.datasource.sqlite

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.util.Log.e
import pl.gawronlucas.clockee.data.datasource.*
import pl.gawronlucas.clockee.data.datasource.sqlite.SqLiteTable.Companion.IN
import pl.gawronlucas.clockee.data.entitiy.AlarmEntity

/**
 * Created by lucas on 30.04.17.
 */
class AlarmSqLiteDataSource(val db: SQLiteDatabase) : AlarmDataSource {

    val alarmTable = AlarmTable()
    val TAG = this.javaClass.simpleName!!

    override fun getAlarmEntity(id: Long): AlarmEntity {
        val cursor = db.query(alarmTable.name(), null,
                "${AlarmTable.KEY_ID} = ?", arrayOf("$id"), null, null, null)
        cursor.moveToFirst()
        val entity = getAlarmEntity(cursor)
        cursor.close()
        return entity
    }

    override fun getAlarmEntityList(ids: Array<Long>): Iterable<AlarmEntity> {
        val idStringArray = Array(ids.size, { ids[it].toString() })
        val cursor = db.query(alarmTable.name(), null,
                "${AlarmTable.KEY_ID} $IN ${DbUtils.createSelectionArgsList(ids.size)}",
                idStringArray, null, null, null)
        cursor.moveToFirst()
        val resultList = mutableListOf<AlarmEntity>()
        for (i in 0 until ids.size) {
            val entity = getAlarmEntity(cursor)
            resultList.add(entity)
            if (!cursor.moveToNext()) break
        }
        cursor.close()
        return resultList
    }

    override fun getAllAlarmEntities(): Iterable<AlarmEntity> {
        val cursor = db.query(alarmTable.name(), null, null, null, null, null, null)
        val resultList = mutableListOf<AlarmEntity>()
        while (cursor.moveToNext()) {
            val entity = getAlarmEntity(cursor)
            resultList.add(entity)
        }
        cursor.close()
        return resultList
    }

    override fun putAlarmEntityList(entities: Iterable<AlarmEntity>) {
        db.beginTransaction()
        for (i in 0 until entities.count()) {
            val alarmEntity = entities.elementAt(i)
            val values = createContentValues(alarmEntity)
            val updated = db.update(alarmTable.name(), values, "${AlarmTable.KEY_ID} = ?",
                    arrayOf(alarmEntity.id.toString()))
            if (updated != 1) {
                val inserted = db.insert(alarmTable.name(), null, values)
                if (inserted == -1L) {
                    e(TAG, "Could not put AlarmEntity from position $i")
                    db.endTransaction()
                    return
                }
            }
        }
        db.setTransactionSuccessful()
        db.endTransaction()
    }

    override fun putAlarmEntity(entity: AlarmEntity) {
        db.beginTransaction()
        val values = createContentValues(entity)
        val updated = db.update(alarmTable.name(), values, "${AlarmTable.KEY_ID} = ?",
                arrayOf(entity.id.toString()))
        if (updated != 1) {
            val inserted = db.insert(alarmTable.name(), null, values)
            if (inserted == -1L) {
                e(TAG, "Could not put AlarmEntity")
                db.endTransaction()
                return
            }
        }
        db.setTransactionSuccessful()
        db.endTransaction()
    }

    override fun deleteAlarmEntityList(ids: Array<Long>) {
        db.beginTransaction()
        for (i in 0 until ids.size) {
            db.delete(alarmTable.name(), "${AlarmTable.KEY_ID} = ?", arrayOf(ids[i].toString()))
        }
        db.setTransactionSuccessful()
        db.endTransaction()
    }

    override fun deleteAlarmEntity(id: Long) {
        db.delete(alarmTable.name(), "${AlarmTable.KEY_ID} = ?", arrayOf(id.toString()))
    }

    /**
     * Internal method to create [AlarmEntity] from cursor. This method do not
     * closes cursor.
     */
    private fun getAlarmEntity(cursor: Cursor): AlarmEntity {
        val enabled = cursor.getInt(AlarmTable.KEY_ENABLED) == 1
        val vibration = cursor.getInt(AlarmTable.KEY_VIBRATION) == 1
        return AlarmEntity(
                cursor.getLong(AlarmTable.KEY_ID),
                cursor.getInt(AlarmTable.KEY_HOUR),
                cursor.getInt(AlarmTable.KEY_MINUTE),
                cursor.getString(AlarmTable.KEY_DAYS),
                enabled,
                vibration,
                cursor.getString(AlarmTable.KEY_RINGTONE_URI),
                cursor.getString(AlarmTable.KEY_NAME)
        )
    }

    private fun createContentValues(entity: AlarmEntity): ContentValues {
        val values = ContentValues()
        values.put(AlarmTable.KEY_ID, entity.id)
        values.put(AlarmTable.KEY_HOUR, entity.hour)
        values.put(AlarmTable.KEY_MINUTE, entity.minute)
        values.put(AlarmTable.KEY_DAYS, entity.days)
        values.put(AlarmTable.KEY_ENABLED, entity.enabled)
        values.put(AlarmTable.KEY_VIBRATION, entity.vibration)
        values.put(AlarmTable.KEY_RINGTONE_URI, entity.ringtoneUri)
        values.put(AlarmTable.KEY_NAME, entity.name)
        return values
    }
}