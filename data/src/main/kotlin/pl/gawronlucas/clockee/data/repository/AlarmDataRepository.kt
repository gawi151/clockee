package pl.gawronlucas.clockee.data.repository

import pl.gawronlucas.clockee.data.datasource.AlarmDataSource
import pl.gawronlucas.clockee.data.entitiy.AlarmEntity
import pl.gawronlucas.clockee.data.mapper.AlarmEntityToAlarmMapper
import pl.gawronlucas.clockee.data.mapper.AlarmToAlarmEntityMapper
import pl.gawronlucas.clockee.domain.Alarm
import pl.gawronlucas.clockee.domain.AlarmRepository

/**
 * Created by Damian Zawadzki on 05.01.2017.
 */
class AlarmDataRepository(val dataStore: AlarmDataSource,
                          val alarmMapper: AlarmToAlarmEntityMapper,
                          val alarmEntityMapper: AlarmEntityToAlarmMapper) : AlarmRepository {
    override fun get(ids: Array<Long>): List<Alarm> {
        val alarmEntities = dataStore.getAlarmEntityList(ids)
        return alarmEntities.map { mapToAlarm(it) }
    }

    override fun get(id: Long): Alarm {
        val entity = dataStore.getAlarmEntity(id)
        return mapToAlarm(entity)
    }

    override fun getAll(): Iterable<Alarm> {
        val alarmEntities = dataStore.getAllAlarmEntities()
        return alarmEntities.map { mapToAlarm(it) }
    }

    override fun save(item: Alarm) {
        val entity = mapToAlarmEntity(item)
        dataStore.putAlarmEntity(entity)
    }

    override fun delete(item: Alarm) {
        dataStore.deleteAlarmEntity(item.alarmId)
    }

    private fun mapToAlarmEntity(alarm: Alarm): AlarmEntity {
        return alarmMapper.mapTo(alarm)
    }

    private fun mapToAlarm(alarmEntity: AlarmEntity): Alarm {
        return alarmEntityMapper.mapTo(alarmEntity)
    }
}