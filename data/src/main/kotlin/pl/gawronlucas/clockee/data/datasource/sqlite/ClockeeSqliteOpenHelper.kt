package pl.gawronlucas.clockee.data.datasource.sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.sql.SQLException

/**
 * Created by lucas on 02.05.17.
 */
class ClockeeSqliteOpenHelper(context: Context, val registeredTables: Iterable<SqLiteTable>) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.beginTransaction()
        registeredTables.forEach {
            db.execSQL(it.createStatement())
        }
        db.setTransactionSuccessful()
        db.endTransaction()
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion != newVersion) {
            // Simplest implementation is to drop all old tables and recreate them
            var success = true
            db.beginTransaction()
            try {
                registeredTables.forEach {
                    db.execSQL("DROP TABLE IF EXISTS " + it.name())
                }
                db.setTransactionSuccessful()
            } catch (e: SQLException) {
                Log.e("ClockeeSqliteOpenHelper", "Upgrading db unsuccessful. Reverting upgrade db transaction.")
                success = false
            } finally {
                db.endTransaction()
            }
            if (success) onCreate(db)
        }
    }

    companion object {
        const val DATABASE_NAME = "clockeeDb"
        const val DATABASE_VERSION = 1
    }
}