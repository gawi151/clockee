package pl.gawronlucas.clockee.data.datasource

import android.database.Cursor

/**
 * Created by lucas on 04.05.17.
 */
fun Cursor.getString(columnName: String): String {
    return this.getString(this.getColumnIndex(columnName))
}

fun Cursor.getInt(columnName: String): Int {
    return this.getInt(this.getColumnIndex(columnName))
}

fun Cursor.getLong(columnName: String): Long {
    return this.getLong(this.getColumnIndex(columnName))
}

class DbUtils {
    companion object {
        /**
         * Creates string with list like (?, ?, ?, ...) for selection arguments in db query.
         */
        fun createSelectionArgsList(numberOfArgs: Int): String {
            if (numberOfArgs <= 0) return ""

            val listBuilder = StringBuilder()
            listBuilder.append("(")
            (0 until numberOfArgs).forEach {
                listBuilder.append("?")
                if (it != numberOfArgs - 1) listBuilder.append(",")
            }
            listBuilder.append(")")
            return listBuilder.toString()
        }
    }
}