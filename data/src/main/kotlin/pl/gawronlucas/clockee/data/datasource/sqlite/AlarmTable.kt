package pl.gawronlucas.clockee.data.datasource.sqlite

import pl.gawronlucas.clockee.data.datasource.sqlite.SqLiteTable.Companion.CREATE_TABLE
import pl.gawronlucas.clockee.data.datasource.sqlite.SqLiteTable.Companion.INTEGER
import pl.gawronlucas.clockee.data.datasource.sqlite.SqLiteTable.Companion.PRIMARY_KEY
import pl.gawronlucas.clockee.data.datasource.sqlite.SqLiteTable.Companion.TEXT

/**
 * Created by lucas on 02.05.17.
 */
class AlarmTable : SqLiteTable {
    override fun createStatement(): String {
        return "$CREATE_TABLE ${name()} (" +
                "$KEY_ID $INTEGER $PRIMARY_KEY," +
                "$KEY_HOUR $INTEGER," +
                "$KEY_MINUTE $INTEGER," +
                "$KEY_DAYS $TEXT," +
                "$KEY_ENABLED $INTEGER," +
                "$KEY_VIBRATION $INTEGER," +
                "$KEY_RINGTONE_URI $TEXT," +
                "$KEY_NAME $TEXT" +
                ")"
    }

    override fun updateStatement(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun columns(): Array<String> {
        return arrayOf(KEY_ID,
                KEY_HOUR,
                KEY_MINUTE,
                KEY_DAYS,
                KEY_ENABLED,
                KEY_VIBRATION,
                KEY_RINGTONE_URI,
                KEY_NAME)
    }

    override fun name(): String = "alarms"

    companion object {
        const val KEY_ID = "id"
        const val KEY_HOUR = "hour"
        const val KEY_MINUTE = "minute"
        const val KEY_DAYS = "days"
        const val KEY_ENABLED = "enabled"
        const val KEY_VIBRATION = "vibration"
        const val KEY_RINGTONE_URI = "ringtone_uri"
        const val KEY_NAME = "name"
    }
}