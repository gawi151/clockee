package pl.gawronlucas.clockee.data.mapper

import pl.gawronlucas.clockee.data.entitiy.AlarmEntity
import pl.gawronlucas.clockee.domain.Alarm
import pl.gawronlucas.clockee.domain.Mapper
import pl.gawronlucas.clockee.domain.Period

/**
 * Created by lucas on 30.04.17.
 */
class AlarmToAlarmEntityMapper : Mapper<Alarm, AlarmEntity> {
    override fun mapTo(t: Alarm): AlarmEntity {
        return AlarmEntity(t.alarmId, t.time.hour, t.time.minutes, periodToString(t.period),
                t.enabled, t.vibration, t.ringtoneUriString, t.name)
    }

    private fun periodToString(period: Period): String {
        TODO("not implemented")
    }
}