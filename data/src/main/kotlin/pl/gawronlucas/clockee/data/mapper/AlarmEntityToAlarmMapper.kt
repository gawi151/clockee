package pl.gawronlucas.clockee.data.mapper

import pl.gawronlucas.clockee.data.entitiy.AlarmEntity
import pl.gawronlucas.clockee.domain.Alarm
import pl.gawronlucas.clockee.domain.AlarmTime
import pl.gawronlucas.clockee.domain.Mapper
import pl.gawronlucas.clockee.domain.Period

/**
 * Created by lucas on 30.04.17.
 */
class AlarmEntityToAlarmMapper : Mapper<AlarmEntity, Alarm> {
    override fun mapTo(t: AlarmEntity): Alarm {
        return Alarm(t.id, AlarmTime(t.hour, t.minute), periodFromString(t.days), t.enabled, t.vibration, t.ringtoneUri, t.name)
    }

    private fun periodFromString(days: String): Period {
        TODO("not implemented")
    }
}