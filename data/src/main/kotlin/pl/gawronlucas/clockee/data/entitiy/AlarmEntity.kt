package pl.gawronlucas.clockee.data.entitiy

/**
 * Created by Damian Zawadzki on 05.01.2017.
 */
data class AlarmEntity(var id: Long = -1, var hour: Int, var minute: Int, var days: String,
                       var enabled: Boolean, var vibration: Boolean, var ringtoneUri: String,
                       var name: String)