package pl.gawronlucas.clockee.data.datasource

import pl.gawronlucas.clockee.data.entitiy.AlarmEntity

/**
 * Created by lucas on 29.04.17.
 */
interface AlarmDataSource {
    fun getAlarmEntity(id: Long): AlarmEntity
    fun getAlarmEntityList(ids: Array<Long>): Iterable<AlarmEntity>
    fun getAllAlarmEntities(): Iterable<AlarmEntity>
    fun putAlarmEntity(entity: AlarmEntity)
    fun putAlarmEntityList(entities: Iterable<AlarmEntity>)
    fun deleteAlarmEntity(id: Long)
    fun deleteAlarmEntityList(ids: Array<Long>)
}