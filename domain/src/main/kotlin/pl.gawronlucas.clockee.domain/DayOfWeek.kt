package pl.gawronlucas.clockee.domain

/**
 * Created by Lucas on 2017-01-02.
 */
enum class DayOfWeek(val num: Int) {
    MONDAY(0), TUESDAY(1), WEDNESDAY(2), THURSDAY(3), FRIDAY(4), SATURDAY(5), SUNDAY(6);
}