package pl.gawronlucas.clockee.domain

/**
 * Created by Lucas on 2017-01-02.
 */
interface AlarmRepository : Repository<Alarm, Long> {
    fun getAll(): Iterable<Alarm>
}