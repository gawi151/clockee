package pl.gawronlucas.clockee.domain

/**
 * Created by Lucas on 2017-01-02.
 */
interface Repository<T, K> {
    fun get(id: K): T
    fun get(ids: Array<K>): Iterable<T>
    fun save(item: T)
    fun delete(item: T)
}