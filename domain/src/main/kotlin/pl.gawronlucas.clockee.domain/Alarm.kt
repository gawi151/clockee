package pl.gawronlucas.clockee.domain

/**
 * Represents alarm object.
 * New [Alarm] is created with empty [Period] - it means that alarm is not periodic - will run once.
 * Non empty [Period] will set alarm as periodic - means that alarm will repeat itself
 * at selected days of the week.
 *
 * Created by Lucas on 2017-01-02.
 */
data class Alarm(var alarmId: Long = -1,
                 var time: AlarmTime = AlarmTime(),
                 var period: Period = Period(),
                 var enabled: Boolean = true,
                 var vibration: Boolean = true,
                 var ringtoneUriString: String = "",
                 var name: String = "")