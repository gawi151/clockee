package pl.gawronlucas.clockee.domain.interactor

import io.reactivex.observers.DisposableObserver

/**
 * Created by lucas on 28.04.17.
 */
interface UseCase<Obs, in Params> {

    /**
     * Executes the current use case.
     *
     * @param observer [DisposableObserver] which will be listening to the observable build
     * @param params Parameters (Optional) used to build/execute this use case.
     */
    fun execute(observer: DisposableObserver<Obs>, params: Params?)

    /**
     * Dispose current [UseCase]. Release resources, stop connections etc.
     */
    fun dispose()
}