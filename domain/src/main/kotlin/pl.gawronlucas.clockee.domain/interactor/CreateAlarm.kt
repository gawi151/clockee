package pl.gawronlucas.clockee.domain.interactor

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import pl.gawronlucas.clockee.domain.Alarm
import pl.gawronlucas.clockee.domain.AlarmRepository
import pl.gawronlucas.clockee.domain.AlarmTime
import java.util.concurrent.Executor

/**
 * Created by lucas on 20.05.17.
 */
class CreateAlarm(val repository: AlarmRepository, workExecutor: Executor, postExecutionTread: Executor, disposables: CompositeDisposable)
    : AbsUseCase<Alarm, CreateAlarm.Params>(workExecutor, postExecutionTread, disposables) {

    override fun buildUseCaseObservable(params: CreateAlarm.Params?): Observable<Alarm> {
        val alarmTime = AlarmTime(params?.hour ?: 0, params?.minute ?: 0)
        val newAlarm = Alarm(time = alarmTime)
        return Observable
                .fromCallable { repository.save(newAlarm) }
                .map { newAlarm }
    }

    data class Params(val hour: Int, val minute: Int)
}