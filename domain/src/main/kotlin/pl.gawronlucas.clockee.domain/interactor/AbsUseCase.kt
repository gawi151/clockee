package pl.gawronlucas.clockee.domain.interactor

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.Executor

/**
 * Created by Damian Zawadzki on 05.01.2017.
 *
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This interface represents a execution unit for different use cases (this means any use case
 * in the application should implement this contract).
 *
 * By convention each AbsUseCase implementation will return the result using a [DisposableObserver]
 * that will execute its job on a [working thread][workExecutor] and will post the result in the [post thread][postExecutionTread].
 *
 */
abstract class AbsUseCase<Obs, in Params>(val workExecutor: Executor, val postExecutionTread: Executor,
                                          val disposables: CompositeDisposable = CompositeDisposable()) : UseCase<Obs, Params> {
    /**
     * Builds an [Observable] which will be used when executing the current {@link AbsUseCase}.
     */
    abstract fun buildUseCaseObservable(params: Params?): Observable<Obs>

    /**
     * Executes the current use case.
     *
     * @param observer [DisposableObserver] which will be listening to the observable build
     * by [AbsUseCase.buildUseCaseObservable] method.
     * @param params Parameters (Optional) used to build/execute this use case.
     */
    override fun execute(observer: DisposableObserver<Obs>, params: Params?) {
        buildUseCaseObservable(params)
                .subscribeOn(Schedulers.from(workExecutor))
                .observeOn(Schedulers.from(postExecutionTread))
                .subscribe(observer)
        addDisposable(observer)
    }

    /**
     * Dispose from current [CompositeDisposable].
     */
    override fun dispose() {
        disposables.dispose()
    }

    /**
     * Adds disposable observer to disposables.
     *
     * @param disposable [DisposableObserver] disposable to add.
     */
    fun addDisposable(disposable: DisposableObserver<Obs>) {
        disposables.add(disposable)
    }
}