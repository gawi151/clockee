package pl.gawronlucas.clockee.domain

import pl.gawronlucas.clockee.domain.util.Capacity
import java.util.*

/**
 * Represents days at which alarm should be run off.
 * Default [Period] is empty. (no period)
 * Each [day][DayOfWeek] can appear only once in [Period]. (no duplicates allowed)
 *
 * Created by Lucas on 2017-01-02.
 */
data class Period(@Capacity(7) var days: EnumSet<DayOfWeek> = EnumSet.noneOf(DayOfWeek::class.java)) {
    fun setRepeatable(repeat: Boolean) {
        days = if (repeat) {
            EnumSet.allOf(DayOfWeek::class.java)
        } else {
            EnumSet.noneOf(DayOfWeek::class.java)
        }
    }

    fun isRepeatable(): Boolean {
        return days.isNotEmpty()
    }
}