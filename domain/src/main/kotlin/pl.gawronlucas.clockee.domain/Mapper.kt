package pl.gawronlucas.clockee.domain

/**
 * Created by Lucas on 2017-01-02.
 */
interface Mapper<in T, out R> {
    fun mapTo(t: T): R
}