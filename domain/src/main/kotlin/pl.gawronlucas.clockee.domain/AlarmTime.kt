package pl.gawronlucas.clockee.domain

import pl.gawronlucas.clockee.domain.util.IntRange

/**
 * Created by Lucas on 2017-01-02.
 */
data class AlarmTime(@IntRange(0, 23) var hour: Int = 0,
                     @IntRange(0, 59) var minutes: Int = 0) {

    override fun toString(): String {
        return "AlarmTime[$hour:$minutes]"
    }
}