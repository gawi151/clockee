package pl.gawronlucas.clockee.domain.util

/**
 * Created by Lucas on 2017-01-02.
 */
@Retention(AnnotationRetention.SOURCE)
annotation class IntRange(val from: Long, val to: Long)