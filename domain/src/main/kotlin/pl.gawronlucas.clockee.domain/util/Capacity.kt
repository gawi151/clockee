package pl.gawronlucas.clockee.domain.util

/**
 * Created by Lucas on 2017-01-02.
 */
@Retention(AnnotationRetention.SOURCE)
annotation class Capacity(val capacity: Long = 0)